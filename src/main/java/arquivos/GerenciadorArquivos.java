package arquivos;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class GerenciadorArquivos {

	public static List<String> lerContas(){
		Path path = Paths.get("src/accounts.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}


		return lista;
	}
	
	
	public static List<String> lerSaques(){
		Path path = Paths.get("src/withdrawals.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}


		return lista;
	}
	
}
