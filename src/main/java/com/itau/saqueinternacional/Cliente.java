package com.itau.saqueinternacional;

import java.util.List;

import arquivos.GerenciadorArquivos;

public class Cliente {
	GerenciadorArquivos arquivo = new GerenciadorArquivos();

	public boolean realizarSaque() {
		List<String> listaSaque = arquivo.lerSaques();

		for (int i = 1; i < listaSaque.size(); i++) {
			String linhaSaque = listaSaque.get(i);
			String[] partes = linhaSaque.split(",");

			int valorSaque = Integer.parseInt(partes[1]);
			String usuario = partes[0];
			String moeda = partes[2];
			int posicaoConta = achouConta(usuario);
			
			double valorSaqueBRL = ConversaoMoeda.getConversao(valorSaque, moeda);
			
			if (posicaoConta < 1) {
				return false;
			} else if(saldoConta(posicaoConta) >= valorSaqueBRL) {
				System.out.println(valorSaqueBRL);
				return true;
			}
		}
		return false;
	}

	private int achouConta(String usuario) {
		List<String> listaConta = arquivo.lerContas();

		for (int i = 1; i < listaConta.size(); i++) {
			String linhaConta = listaConta.get(i);
			String[] partes = linhaConta.split(",");

			int saldo = Integer.parseInt(partes[3]);
			String cliente = partes[1];
			String tipo = partes[4];
			if (cliente.equals(usuario)) {
				return i;
			}
		}
		System.out.println("Conta não existe");
		return -1;
	}
	
	private int saldoConta(int posicaoConta) {
		List<String> listaConta = arquivo.lerContas();
		String linhaConta = listaConta.get(posicaoConta);
		String[] partes = linhaConta.split(",");
		return Integer.parseInt(partes[3]);
	}
	
	// metodo saque
}
