package com.itau.saqueinternacional;

import java.util.List;

import arquivos.GerenciadorArquivos;
import leitorApi.ContacaoMoeda;

public class App 
{
    public static void main( String[] args )
    {

        double cotacao = ContacaoMoeda.getCotacaoBRL();
             
        System.out.println(cotacao);
        
        Cliente cliente = new Cliente();
    
        System.out.println(cliente.realizarSaque());

    }
}
