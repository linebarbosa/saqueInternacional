package com.itau.saqueinternacional;

import leitorApi.ContacaoMoeda;

public class ConversaoMoeda {

	public static double getConversao (int valor, String moeda) {
		if (moeda.equals("BRL")) {
			return valor;
		} else {
	        double cotacao = ContacaoMoeda.getCotacaoBRL();
	        double valorCalculado = valor * cotacao;
	        return valorCalculado;
		}
	}
}
